# the church encoding

---

### `() => {}`

---

no brackets required

```js
var a = (x) => { return x + 1 };

var b = x => x + 1;

a(4) // 5
b(4) // 5
```
---

implicit returns

```js
var a = () => 1;

a() // 1
```

---

first class citizen
```js
var a = x => x + 1;

var b = y => y(4) + 1;

b(a) // 6
```

---

## booleans/conditionals

---

```js
if (someFunction(value)) {
  console.log('it returned `true`');
} else {
  console.log('it returned `false`');
}
```

---

```smalltalk
(someObject someMethod)
  ifTrue:  [ printnl "it returned `true`" ];
  ifFalse: [ printnl "it returned `false`" ]
```

---

```js
someObject.somefunction()
  .ifTrue(() => { console.log('it returned `true`')})
  .ifFalse(() => { console.log('it returned `false`')});
```

---

```js
if (someFunction(value)) {
  console.log('it returned `true`');
} else {
  console.log('it returned `false`');
}
```
```js
someObject.somefunction()
  .ifTrue(() => { console.log('it returned `true`')})
  .ifFalse(() => { console.log('it returned `false`')});
```

---

```js
somefunction()(
  () => { console.log('it returned `true`')},
  () => { console.log('it returned `false`')}
);
```

---

```js
var True = (trueFn, falseFn) => trueFn();
var False = (trueFn, falseFn) => falseFn();
```
---

```js
var True = (trueFn, falseFn) => trueFn();

True(
  () => { console.log('`True` is true') },
  () => { console.log('`True` is false') },
);

// `True` is true
```

---

### lambda calculus?

---
`λx.x+1 `

`x => x + 1`
---

### ONLY ONE ARGUMENT?

---

```js
var a = (x, y) => (x + y);

var b0 = (x) => { return (y) => { return x + y }  };

var b = x => y => x + y;

a(2, 3); // 5
b(2)(3); // 5
```
---

```js
var oldTrue = (trueAction, falseAction) => trueAction();
var True = trueAction => falseAction => trueAction();

oldTrue(
  () => console.log('true'),
  () => console.log('false')
)
// true

True(
  () => console.log('true')
)(
  () => console.log('false)
)
// true
```

---

### converting to something real

---

```js
var toBool = boolean => {
  return boolean(
    () => true
  )(
    () => {false}
  )
}
```

---

```js
var True  = trueAction => falseAction => trueAction();
var False = trueAction => falseAction => falseAction();

var toBool = boolean => boolean(() => true)(() => {false})

toBool(True)  // true
toBool(False) // false
```

---
### negation
---

```js
var True  = trueAction => falseAction => trueAction();
var False = trueAction => falseAction => falseAction();

var not = boolean => {
  return boolean(
    () => False
  )(
    () => True
  )
}
```

---

```js
var True  = trueAction => falseAction => trueAction();
var False = trueAction => falseAction => falseAction();

var not = boolean => boolean(() => False)(() => True)

toBool(not(True))  // false
toBool(not(False)) // true
```

---
### and
---

```js
true  && true  = true
true  && false = false
false && true  = false
false && false = false
```

---

```js
var True  = trueAction => falseAction => trueAction();
var False = trueAction => falseAction => falseAction();

var and = (bool1, bool2) => {
  return trueAction => falseAction => {
    return bool1(
      bool2(trueAction())(falseAction())
    )(
      falseAction()
    )
  }
}
```

---


### numbers

---

```js
var one = (action) => (value) => action(value)
```

---

```js
var one = action => value => action(value)
var two = action => value => action(action(valueue))


// ...


var nine = action => value => action(action(action(action(action(action(action(action(action(valueue)))))))))
```

---

```js
var toInt = number => number(value => value + 1)(0)
```
---
### the successor
---

```js
var successor = (number) => {
  return action => value => {
    var numberApplied = number(action)(value)
    return action(numberApplied)
  }
}
```

---

```js
var successor = (number) => {
  return action => value => {
    var numberApplied = number(action)(valueue)
    return action(numberApplied)
  }
}

var successor = number => {
   return action => value => action(number(action)(value))
}
```
---

```js

var successor = number => {
   return action => value => action(number(action)(value))
}

var two   = action => value => action(action(value))
var three = action => value => action(action(action(value)))

var plus = num1 => num2 => {
  return action => value => {
    num1Applied = num1(action)(value)
    num2Applied = num2(action)(num1Applied)
  }
}
```
---

```js

var successor = number => {
   return action => value => action(number(action)(value))
}

var two   = action => value => action(action(value))
var three = action => value => action(action(action(value)))

var plus = num1 => num2 => {
  return action => value => num2(action)(num1(action)(value))
}
```


---
```js

var successor = number => {
  return action => val => action(number(action)(value))
}

var two   = action => value => action(action(value))
var three = action => value => action(action(action(value)))


var plus = num1 => num2 => {
  return num1(successor)(num2)
}
```

---
multiplication
---

```js
var two   = action => value => action(action(value))
var three = action => value => action(action(action(value)))

var multiply = num1 => num2 => {
  return action => value => {
    var newFunc = num1(action)
    return num2(newFunc)(value)
  }
}

```
---

```js
var two   = action => value => action(action(value))
var three = action => value => action(action(action(value)))

var multiply = num1 => num2 => {
  return action => value => num2(num1(action))(value)
}
```

---


